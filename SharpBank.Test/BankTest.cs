﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        [Test]
        public void TestBankCustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John").OpenAccount(new CheckingAccount());
            bank.AddCustomer(john);
            Customer mark = new Customer("Mark").OpenAccount(new CheckingAccount()).OpenAccount(new SavingsAccount());
            bank.AddCustomer(mark);

            string summary = bank.CustomerSummary();

            Assert.AreEqual(summary, "Customer Summary\n - John (1 account)\n - Mark (2 accounts)", summary);
        }

        [Test]
        public void TestBankCheckingAccountTotalInterest()
        {
            Bank bank = new Bank();
            IAccount account = new CheckingAccount();
            Customer bill = new Customer("Bill").OpenAccount(account);
            bank.AddCustomer(bill);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(300.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(200.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(300.0);

            var totalInterest = bank.TotalInterestPaid();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(totalInterest, 0.06301, Constants.DOUBLE_INT_DELTA);
        }

        [Test]
        public void TestbankSavingsAccountTotalInterest()
        {
            Bank bank = new Bank();
            IAccount account = new SavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(account));
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(4000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(3000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(1000.0);

            var totalInterest = bank.TotalInterestPaid();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(totalInterest, 1.1506, Constants.DOUBLE_INT_DELTA);
        }

        [Test]
        public void TestBankMaxiSavingsAccountTotalInterest()
        {
            Bank bank = new Bank();
            IAccount account = new MaxiSavingsAccount();
            bank.AddCustomer(new Customer("Bill").OpenAccount(account));
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(2000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(500.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Withdraw(1000.0);

            var totalInterest = bank.TotalInterestPaid();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(totalInterest, 13.8767, Constants.DOUBLE_INT_DELTA);
        }
    }

}
