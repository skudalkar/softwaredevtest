﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void TestCustomerStatementGeneration()
        {
            IAccount checkingAccount = new CheckingAccount();
            IAccount savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            string statement = henry.GetStatement();

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", statement);
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new SavingsAccount());
            oscar.OpenAccount(new CheckingAccount());

            int accountCount = oscar.GetNumberOfAccounts();

            Assert.AreEqual(2, accountCount);
        }

        [Test]
        public void TestZeroAcounts()
        {
            Customer oscar = new Customer("Oscar");

            int accountCount = oscar.GetNumberOfAccounts();

            Assert.AreEqual(0, accountCount);
        }

        [Test]
        public void TestTotalInterestEarned()
        {
            IAccount checkingAccount = new CheckingAccount();
            IAccount savingsAccount = new SavingsAccount();
            IAccount maxiSavingsAccount = new MaxiSavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount).OpenAccount(maxiSavingsAccount);

            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            checkingAccount.Deposit(300.0);
            savingsAccount.Deposit(4000.0);
            maxiSavingsAccount.Deposit(2000.0);

            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            checkingAccount.Withdraw(200.0);
            savingsAccount.Withdraw(3000.0);
            maxiSavingsAccount.Withdraw(500.0);

            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            checkingAccount.Deposit(300.0);
            savingsAccount.Deposit(1000.0);
            maxiSavingsAccount.Withdraw(1000.0);

            double totalInterstEarned = henry.TotalInterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(totalInterstEarned, 15.0904, Constants.DOUBLE_INT_DELTA);

        }

        [Test]
        public void TestAccountTransfer()
        {
            IAccount checkingAccount = new CheckingAccount();
            IAccount savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            checkingAccount.Deposit(1000.0);
            savingsAccount.Deposit(2000.0);

            henry.Transfer(checkingAccount, savingsAccount, 100);

            Assert.AreEqual(checkingAccount.GetBalance(), 900.0, Constants.DOUBLE_DELTA);
            Assert.AreEqual(savingsAccount.GetBalance(), 2100.0, Constants.DOUBLE_DELTA);

        }
        [Test]
        public void TestAccountTransferInsufficientFunds()
        {
            IAccount checkingAccount = new CheckingAccount();
            IAccount savingsAccount = new SavingsAccount();
            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);
            checkingAccount.Deposit(1000.0);
            savingsAccount.Deposit(1000.0);

            Assert.Throws<ApplicationException>(() => henry.Transfer(checkingAccount, savingsAccount, 2000));

        }
    }
}
