﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void TestCheckingAccountName()
        {
            IAccount account = new CheckingAccount();
            string name = account.Name;
            Assert.AreEqual(name, "Checking Account");
        }
        [Test]
        public void TestSavingsAccountName()
        {
            IAccount account = new SavingsAccount();
            string name = account.Name;
            Assert.AreEqual(name, "Savings Account");
        }
        [Test]
        public void TestMaxiSavingsAccountName()
        {
            IAccount account = new MaxiSavingsAccount();
            string name = account.Name;
            Assert.AreEqual(name, "Maxi Savings Account");
        }
        [Test]
        public void TestAccountDeposit()
        {
            IAccount account = new CheckingAccount();
            account.Deposit(4.0);
            Assert.AreEqual(account.GetBalance(), 4.0, Constants.DOUBLE_DELTA);
        }
        [Test]
        public void TestAccountWithdraw()
        {
            IAccount account = new SavingsAccount();
            account.Deposit(40.0);
            account.Withdraw(20.0);
            Assert.AreEqual(account.GetBalance(), 20.0, Constants.DOUBLE_DELTA);
        }
        [Test]
        public void TestDepositArgumentException()
        {
            IAccount account = new MaxiSavingsAccount();

            Assert.Throws<ArgumentException>(() => account.Withdraw(0));
        }
        [Test]
        public void TestWithdrawArumentException()
        {
            IAccount account = new MaxiSavingsAccount();

            Assert.Throws<ArgumentException>(() => account.Deposit(0));
        }
        [Test]
        public void TestBalanceCalculation()
        {
            IAccount account = new CheckingAccount();
            account.Deposit(3.0);
            account.Deposit(11.4);
            account.Withdraw(4.0);

            double balance = account.GetBalance();

            Assert.AreEqual(balance, 10.4, Constants.DOUBLE_DELTA);
        }
        public void TestAccountStatement()
        {
            IAccount savingsAccount = new SavingsAccount();
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            string statement = savingsAccount.GetStatement();

            Assert.AreEqual(statement, 
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n");
        }

        [Test]
        public void TestCheckingAccountInterestEarned()
        {
            IAccount account = new CheckingAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(300.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(200.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(300.0);

            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 0.0630, Constants.DOUBLE_INT_DELTA);
        }
        [Test]
        public void TestSavingsAccountInterestEarnedNotOverThousand()
        {
            IAccount account = new SavingsAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(400.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(200.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(300.0);


            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 0.0904, Constants.DOUBLE_INT_DELTA);
        }
        [Test]
        public void TestSavingsAccountInterestEarnedOverThousand()
        {
            IAccount account = new SavingsAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(4000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(3000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(1000.0);

            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 1.1506, Constants.DOUBLE_INT_DELTA);
        }

        [Test]
        public void TestMaxiSavingsAccountInterestEarnedNotOverThousand()
        {
            IAccount account = new MaxiSavingsAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(1000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(200.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Deposit(100.0);

            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 8.1698, Constants.DOUBLE_INT_DELTA);
        }
        [Test]
        public void TestMaxiSavingsAccountInterestEarnedNotOverTwoThousand()
        {
            IAccount account = new MaxiSavingsAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(2000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Withdraw(500.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Withdraw(1000.0);

            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 13.8767, Constants.DOUBLE_INT_DELTA);
        }
        [Test]
        public void TestMaxiSavingsAccountInterestEarnedOverTwoThousand()
        {
            IAccount account = new MaxiSavingsAccount();
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-100));
            account.Deposit(2000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-50));
            account.Deposit(1000.0);
            DateProvider.GetInstance().SetDebugDate(DateTime.Now.AddDays(-10));
            account.Withdraw(500.0);

            double interestEarned = account.InterestEarned();

            DateProvider.GetInstance().ResetDebugDate();

            Assert.AreEqual(interestEarned, 30.2054, Constants.DOUBLE_INT_DELTA);
        }
    }
}
