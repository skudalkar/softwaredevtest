﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Test
{
    public static class Constants
    {
        public static readonly double DOUBLE_DELTA = 1e-15;
        public static readonly double DOUBLE_INT_DELTA = 1e-4;
    }
}
