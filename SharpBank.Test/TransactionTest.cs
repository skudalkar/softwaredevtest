﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class TransactionTest
    {
        [Test]
        public void TestTransactionAmount()
        {
            Transaction t = new Transaction(5.0);
            double amount = t.Amount;
            Assert.AreEqual(t.Amount, 5.0);
        }
        public void TestTransactionDate()
        {
            DateTime dt = DateTime.Now;
            Transaction t = new Transaction(5.0);
            DateTime dtResult = t.TransactionDate;
            Assert.IsTrue(DateTime.Compare(dt, dtResult) <= 0);
        }

    }
}
