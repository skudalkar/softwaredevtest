﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class DateProviderTest
    {
        [Test]
        public void TestDateNow()
        {
            DateProvider dp = DateProvider.GetInstance();
            DateTime dt = DateTime.Now;
            dp.ResetDebugDate();
            DateTime dtResult = dp.Now();
            Assert.IsTrue(DateTime.Compare(dt, dtResult) <= 0);
        }
    }
}
