﻿using System;
using System.Collections.Generic;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<IAccount> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<IAccount>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(IAccount account)
        {
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            if ( accounts != null)
            {
                foreach (IAccount a in accounts)
                    total += a.InterestEarned();
            }
            return total;
        }

        public String GetStatement()
        {
            String statement = null;

            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (IAccount a in accounts)
            {
                statement += "\n" + a.GetStatement() + "\n";
                total += a.GetBalance();
            }
            statement += "\nTotal In All Accounts " + AmountFormat.ToDollars(total);
            return statement;
        }

        public void Transfer(IAccount fromAccount, IAccount toAccount, double amount)
        {
            if (amount < 0)
                throw new ArgumentException("Amount cannot be less than zero.");

            if (fromAccount.GetBalance() < amount)
                throw new ApplicationException("From Account does not have sufficient funds to make the transfer.");

            fromAccount.Withdraw(amount);

            toAccount.Deposit(amount);
        }
    }
}
