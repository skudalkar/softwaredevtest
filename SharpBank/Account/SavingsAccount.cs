﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class SavingsAccount : BaseAccount
    {
        override protected double GetDailyInterstForAmount(double amount)
        {
            if (amount <= 1000)
                return amount * PerDayFraction(0.001);
            else
                return PerDayFraction(1) + (amount - 1000) * PerDayFraction(0.002);
        }
        public override string Name
        {
            get
            {
                return "Savings Account";
            }
        }
    }
}
