﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class CheckingAccount : BaseAccount
    {
        public CheckingAccount() : base()
        {
        }
        override protected double GetDailyInterstForAmount(double amount)
        {
            double dailyInterestRate = PerDayFraction(0.001);
            return dailyInterestRate * amount;
        }
        public override string Name {
            get
            {
                return "Checking Account";
            }
        }
    }
}
