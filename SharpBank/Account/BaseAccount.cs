﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public abstract class BaseAccount : IAccount
    {
        protected List<Transaction> transactions = null;

        public abstract string Name { get; }

        protected BaseAccount()
        {
            transactions = new List<Transaction>();
        }
        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                if (GetBalance() - amount < 0)
                    throw new ArgumentException("Insufficient balance.");
                transactions.Add(new Transaction(-amount));
            }
        }
        public double GetBalance()
        {
            return transactions.Sum(a => a.Amount);
        }

        public string GetStatement()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Name + "\n");

            foreach (Transaction t in transactions)
            {
                sb.Append("  " + (t.Amount < 0 ? "withdrawal" : "deposit") + " " + AmountFormat.ToDollars(t.Amount) + "\n" );
            }

            sb.Append("Total " + AmountFormat.ToDollars(GetBalance()));

            return sb.ToString();
        }

        virtual public double InterestEarned()
        {
            double interestAccrued = 0.0;
            double newBalance = 0.0;

            for (int i = 0; i < transactions.Count; i++)
            {
                Transaction t = transactions[i];
                newBalance += t.Amount;
                int duration = 0;
                if (i + 1 == transactions.Count)
                    duration = GetWholeDaysBetween(t.TransactionDate, DateTime.Now);
                else
                    duration = GetWholeDaysBetween(t.TransactionDate, transactions[i + 1].TransactionDate);

                interestAccrued += GetDailyInterstForAmount(newBalance) * duration;
            }
            return interestAccrued;
        }

        abstract protected double GetDailyInterstForAmount(double amount);

        protected int GetWholeDaysTillToday(DateTime fromDate)
        {
            return (int)DateTime.Now.Subtract(fromDate).TotalDays;
        }
        protected int GetWholeDaysBetween(DateTime fromDate, DateTime toDate)
        {
            return (int)toDate.Subtract(fromDate).TotalDays;
        }

        protected double PerDayFraction(double rate)
        {
            return rate / 365;
        }

    }
}
