﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class MaxiSavingsAccount : BaseAccount
    {
        override public double InterestEarned()
        {
            double interestAccrued = 0.0;
            double newBalance = 0.0;

            for (int i = 0; i < transactions.Count; i++)
            {
                Transaction t = transactions[i];
                newBalance += t.Amount;
                int duration = 0;
                if (i + 1 == transactions.Count)
                    duration = GetWholeDaysBetween(t.TransactionDate, DateTime.Now);
                else
                    duration = GetWholeDaysBetween(t.TransactionDate, transactions[i + 1].TransactionDate);

                bool anyWithdrawalsInPriorTenDays = (t.Amount < 0)
                    || transactions.Any(a => a.TransactionDate < t.TransactionDate && t.Amount < 0 && GetWholeDaysBetween(a.TransactionDate, t.TransactionDate) <= 10);

                double dailyInterestAmount = 0.0;
                if ( anyWithdrawalsInPriorTenDays)
                    dailyInterestAmount = newBalance * PerDayFraction(0.001);
                else
                    dailyInterestAmount = newBalance * PerDayFraction(0.05);

                interestAccrued += dailyInterestAmount * duration;
            }
            return interestAccrued;
        }

        override protected double GetDailyInterstForAmount(double amount)
        {
            throw new NotImplementedException("Maxi Savings does not use this virutal");
        }

        public override string Name
        {
            get
            {
                return "Maxi Savings Account";
            }
        }
    }
}
