﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public interface IAccount
    {
        void Deposit(double amount);
        void Withdraw(double amount);
        double GetBalance();
        string GetStatement();
        double InterestEarned();
        string Name { get; }
    }
}
