﻿using System;

namespace SharpBank
{
    public class DateProvider
    {
        private static DateProvider instance = null;

        private DateTime DebugDate = DateTime.MinValue;

        private DateProvider() { }

        public static DateProvider GetInstance()
        {
            if (instance == null)
                instance = new DateProvider();
            return instance;
        }

        public DateTime Now()
        {
            if (DebugDate > DateTime.MinValue)
                return DebugDate;

            return DateTime.Now;
        }
        /// <summary>
        /// Quick but unsafe way to inject desired dates as it exposes the Date generator to client assemblies.
        /// </summary>
        /// <param name="dt">Date you want the transactions to be generated for.</param>
        public void SetDebugDate(DateTime dt)
        {
            DebugDate = dt;
        }
        public void ResetDebugDate()
        {
            DebugDate = DateTime.MinValue;
        }
    }
}
