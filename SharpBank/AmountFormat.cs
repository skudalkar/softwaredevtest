﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public static class AmountFormat
    {
        public static string ToDollars(double d)
        {
            return string.Format("${0:N2}", Math.Abs(d));
        }
    }
}
