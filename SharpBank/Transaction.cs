﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public readonly double Amount;

        public readonly DateTime TransactionDate;

        public Transaction(double amount)
        {
            this.Amount = amount;
            this.TransactionDate = DateProvider.GetInstance().Now();
        }

    }
}
